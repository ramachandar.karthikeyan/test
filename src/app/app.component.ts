import { Component } from '@angular/core';
import { AccountService } from './account.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title=""
  sidenavOpen:boolean = false;

  constructor(public _accountService:AccountService){
  }

  sidenavToggle(){
    this.sidenavOpen = !this.sidenavOpen;
  }

  ngOnInit() {  
  }

}
