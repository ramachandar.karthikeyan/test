import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EmpService } from '../../emp-service.service';

@Component({
  selector: 'app-emp-details',
  templateUrl: './emp-details.component.html',
  styleUrls: ['./emp-details.component.scss']
})
export class EmpDetailsComponent implements OnInit {

  constructor(public _empService:EmpService,
    @Inject(MAT_DIALOG_DATA) public data:any,
    ) { }

  ngOnInit(): void {
  }

}
