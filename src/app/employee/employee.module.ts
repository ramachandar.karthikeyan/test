import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { EmployeeRoutingModule } from './employee-routing.module';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeFormComponent } from './employee-form/employee-form.component';
import { MaterialModule } from '../material-module';
import { HttpClientModule } from '@angular/common/http';
import { EmpDetailsComponent } from './emp-details/emp-details/emp-details.component';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog/delete-dialog.component';
import {MatDatepickerModule} from '@angular/material/datepicker';

@NgModule({
  declarations: [
    EmployeeListComponent,
    EmployeeFormComponent,
    EmpDetailsComponent,
    DeleteDialogComponent
  ],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    HttpClientModule,
    MatDatepickerModule
  ]
})
export class EmployeeModule { }