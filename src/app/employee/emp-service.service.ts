import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmpService {
  BASEURL = environment.BASEURL

  empID = '';
  updateValue:boolean = false;
  employeeData:any;
  selectedIndex:Number = 0 
  constructor(public _http: HttpClient, private _router: Router) { }

  getAllEmployees(date?:  string) {
    let url = this.BASEURL + "employee/list"
    
    if(date) {
      let dateFormat = new Date(date);
      let FormatDate = `${dateFormat.getFullYear()}-${dateFormat.getMonth()+1}-${dateFormat.getDate()}`
      console.log("Date Fromat", FormatDate)
      url += `?DOJ=${FormatDate}`
    }
    return this._http.get(url);
  }

  createEmployee(URL: string,Body?: any){
      return this._http.post(URL, Body);
  }

  deleteEmployee(URL: string){
      return this._http.delete(URL,);
  }

  updateEmployee(URL: string,Body?: any){
    return this._http.put(URL, Body);
  }





}
