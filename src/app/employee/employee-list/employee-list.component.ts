import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { EmpDetailsComponent } from '../emp-details/emp-details/emp-details.component';
import { EmpService } from '../emp-service.service'
import { DeleteDialogComponent } from '../delete-dialog/delete-dialog/delete-dialog.component';
import { AccountService } from 'src/app/account.service';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';
import { FormControl } from '@angular/forms';
import { MatInput } from '@angular/material/input';


@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {

  @ViewChild('datePick', {
    read: MatInput
  }) datePick!: MatInput;
  
  constructor(private router: Router, public dialog: MatDialog, public empService: EmpService,
    public _accountService: AccountService,
    private toastr: ToastrService,
  ) { }

  displayedColumns: string[] = ['Name', 'Gender', 'DOB', 'DOJ', 'Hobbies', 'Experience', 'Action'];
  dataSource = [];
  tabValue: boolean = false;
  BASEURL = environment.BASEURL

  ngOnInit(): void {
    this._accountService.authenticated = true;
    this.getAllEmployees();
  }

  ngOnChanges() {
    this.empService.updateValue = false;
  }

  HandleDate(eve: any) {
    console.log("Date change event", eve);
    this.empService.getAllEmployees(eve.value).subscribe(
      (res: any) => {
        console.log(res);
        if (res.data) {
          this.dataSource = res.data;
          this.dataSource.map((ele: any) => {
            ele.Hobbies = ele.hobbies;
            ele.Name = ele.name;
            ele.Experience = ele.experience;
            ele.Gender = ele.gender;
          })
        } else {
          this.dataSource = [];
        }
      }
    );
  }

  getAllEmployees() {
    // let requestURL = this.BASEURL+'employee/list'
    this.empService.getAllEmployees().subscribe({
      next: (response: any) => {
        console.log(response);
        this.dataSource = response.data;
        this.dataSource.map((ele: any) => {
          ele.Hobbies = ele.hobbies;
          ele.Name = ele.name;
          ele.Experience = ele.experience;
          ele.Gender = ele.gender;
        })
      },
      error: error => {
        console.error('There was an error!', error);
      }
    });
  }

  edit(event: any) {
    this.empService.selectedIndex = 1;
    this.empService.empID = event.id;
    this.empService.updateValue = true;
    this.empService.employeeData = event;
  }

  clearFilters() {
    this.datePick.value = '';
    this.getAllEmployees()
  }

  deleteEmp(event: any) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '600px',
      height: '200px',
    });
    let ID = event.id;

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let requestURL = this.BASEURL + 'employee/delete/{' + ID + '}'
        this.empService.deleteEmployee(requestURL).subscribe({
          next: (response: any) => {
            console.log(response);
            this.getAllEmployees();
            this.toastr.success("Employee deleted successfully");
          },
          error: error => {
            console.error('There was an error!', error);
          }
        });
      }
    });

  }

  rowClick(event: any) {
    let data = event;
    const dialogRef = this.dialog.open(EmpDetailsComponent, {
      width: '600px',
      height: '500px',
      data
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });

  }

  onTabChanged(event: any) {
    console.log("tab change", event)
    this.empService.selectedIndex = event.index;
    if (this.empService.selectedIndex == 0) {
      this.getAllEmployees();
      this.empService.updateValue = false;
      this.empService.employeeData = '';
      this.tabValue = false;
    } else {
      this.tabValue = true;
    }
  }

  handleTabChange(eve: boolean) {
    this.empService.selectedIndex = 0
    this.getAllEmployees();
      this.empService.updateValue = false;
      this.empService.employeeData = '';
      this.tabValue = false;
  }
}

