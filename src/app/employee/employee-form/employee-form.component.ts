import { Component, Input, Output, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { EmpService } from '../emp-service.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.scss']
})
export class EmployeeFormComponent implements OnInit {


  @Input() tabValue!:any;
  @Output() tabChange: EventEmitter<boolean> = new EventEmitter();
  @ViewChild(FormGroupDirective) formDirective!: FormGroupDirective;


  constructor(public formbuilder:FormBuilder,
              public _empService:EmpService,
              private toastr: ToastrService,
    ) { }

  employeeForm!: FormGroup;
  submitted:boolean = false;
  BASEURL = environment.BASEURL;


  ngOnInit(): void {
    this.intializeForm();
  }

  intializeForm(){
    this.employeeForm = this.formbuilder.group({
      name: ['',Validators.required],
      gender:['',],
      dateofBirth:[null,],
      dateofJoining:[null,],
      hobbies:['',],
      experience:['',],
    });
  }

  ngOnChanges(){
    if(this._empService.updateValue){
      this.intializeForm();
      setTimeout(() => {
        this.patchValue()
      },0)
      
    } else {
      this.employeeForm.reset();
    }  }  

  patchValue(){
    console.log(this._empService.employeeData);
    this.employeeForm.patchValue({
      dateofBirth:this._empService.employeeData.DOB,
      dateofJoining:this._empService.employeeData.DOJ,
      name:this._empService.employeeData.name,
      gender:this._empService.employeeData.gender,
      hobbies:this._empService.employeeData.hobbies,
      experience:this._empService.employeeData.experience
    })
    
    console.log(this.employeeForm.controls['dateofBirth'].value);
  }

  cancel(){
    this.submitted = false;
    this.employeeForm.reset();
    this.formDirective.resetForm();
    this.tabChange.emit(false);
  }

  onSubmit(){
    this.submitted = true;

    if(this.employeeForm.status == 'VALID'){
      let data = {
        name:this.employeeForm.value.name,
        gender:this.employeeForm.value.gender,
        DOB:new Date(this.employeeForm.value.dateofBirth),
        DOJ:new Date(this.employeeForm.value.dateofJoining),
        hobbies:this.employeeForm.value.hobbies,
        experience:this.employeeForm.value.experience,
      }
      console.log("Form Data", data)
  
      let requestURL = this.BASEURL+'employee/create'
      this._empService.createEmployee(requestURL,data).subscribe({
        next: (response: any) => {
          console.log(response);
          this.formDirective.resetForm();
          this.toastr.success("Employee Created successfully");
          },
        error: error => {
            console.error('There was an error!', error);
          }
        });

        this.tabChange.emit(false);
    } else {
      this.toastr.error("Fill the required fields");

    }

  }

  updateEmp(){
    let data = {
      name:this.employeeForm.value.name,
      gender:this.employeeForm.value.gender,
      DOB:this.employeeForm.value.dateofBirth,
      DOJ:this.employeeForm.value.dateofJoining,
      hobbies:this.employeeForm.value.hobbies,
      experience:this.employeeForm.value.experience,
    }
    let ID = this._empService.empID;
    let requestURL = this.BASEURL+'employee/update/{'+ID+'}'
    this._empService.updateEmployee(requestURL,data).subscribe({
      next: (response: any) => {
        console.log(response);
        this._empService.selectedIndex = 0;
        this.toastr.success("Employee updated successfully");

        this.tabChange.emit(false);
        },
      error: error => {
          console.error('There was an error!', error);
        }
      });
  }

}
