import { Injectable, Injector } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from "@angular/router";
// import { AccountService } from './account.service';

@Injectable()
export class HttpInterceptorInterceptor implements HttpInterceptor {

  constructor(
    public inj: Injector,
   private http: HttpClient,
   private router: Router,
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // const auth = this.inj.get(AccountService);
    // let token = auth.token
    let token = localStorage.getItem("token")
    if(token && token !== "null"){
      // console.log("inside clone request with authorization")
      return next.handle(
        request.clone({
          setHeaders: {
            Authorization : `Bearer ${token}`,
          }
        })
      )
    }
    else{
      // console.log("inside the clone request without authorization")
      return next.handle(request.clone())
    }    
    // return next.handle(request.clone())
  }
}
