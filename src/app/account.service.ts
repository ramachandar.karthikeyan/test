import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

interface EncryptPayload {
  encryptPayload: string,
}

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  token: string = "";
  user_ID: string = "";
  BASE_URL: string = environment.BASEURL;
  authenticated:boolean = false;
  userMail: string = "";
  userDetails: any;
  
  constructor(
    private _httpClient: HttpClient,
  ) { }

  createAccount(data: EncryptPayload) {
    let url = this.BASE_URL + "users/create";
    return this._httpClient.post(url, data);
  }

  getUser(data: EncryptPayload) {
    let url = this.BASE_URL + "users/getUser";
    return this._httpClient.post(url, data);
  }

  sendOTP(id: string) {
    let url = this.BASE_URL + "users/sendOTP/" + id;
    return this._httpClient.post(url, {});
  }

  verifyOTP(id: string, body: any) {
    let url = this.BASE_URL + "users/verifyOTP/" + id;
    return this._httpClient.post(url, body);
  }

}
