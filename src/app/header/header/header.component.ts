import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/account.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() SideNavToggle = new EventEmitter();  
  username: string = "";
  constructor(
    private router: Router,
    private accountService: AccountService,
  ) { }

  ngOnInit(): void {
    this.username = this.accountService.userDetails.username;
  }

  openSidenav() {
   this.SideNavToggle.emit();
 }

  onSignOut(){
    this.accountService.authenticated = false;
    localStorage.setItem("token", "");
    localStorage.clear();
    this.router.navigate(['/login']);
 }

}
