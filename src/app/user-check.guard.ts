import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AccountService } from './account.service';

@Injectable({
  providedIn: 'root'
})
export class UserCheckGuard implements CanActivate {
  constructor(
    private _router: Router,
    private accountService: AccountService
    ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      let usermail = this.accountService.userMail;
      let id = this.accountService.user_ID;
      if (usermail && id) {
        return true;
      }
      this._router.navigate(['/login'])
      return false;
  }
  
}
