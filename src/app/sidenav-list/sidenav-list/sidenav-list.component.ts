import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.scss']
})
export class SidenavListComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  route(path:any){
    if(path == 'Dashboard'){
         this.router.navigate(['']);
    }
    if(path == 'Employee'){
         this.router.navigate(['/employee']);
    }
  }

}
