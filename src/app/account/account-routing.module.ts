import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CheckGuard } from '../check.guard';
import { UserCheckGuard } from '../user-check.guard';
import { LoginComponent } from './login/login.component';
import { OTPverifyComponent } from './otpverify/otpverify.component';
import { SignupComponent } from './signup/signup.component';

const routes: Routes = [
  {
    path: 'login',
    canActivate: [CheckGuard],
    component: LoginComponent
  },
  {
    path: 'sign-up',
    canActivate: [CheckGuard],
    component: SignupComponent
  },
  {
    path: 'OTPverfiy',  
    canActivate: [CheckGuard, UserCheckGuard],
    component: OTPverifyComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
