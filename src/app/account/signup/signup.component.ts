import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/account.service';
import { UtilsService } from 'src/app/utils.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  hide: boolean = true;
  hide2: boolean = true;
  showPasswordErr: boolean = false;
  // showOTPcomponent: boolean = false;
  showEmailExistErr: boolean = false;
  // showOTPcomponent: boolean = true;
  signUpForm: FormGroup = new FormGroup({
    name: new FormControl("", [Validators.required]),
    email: new FormControl("", [Validators.required, Validators.email]),
    userName: new FormControl("", [Validators.required]),
    password: new FormControl("", [Validators.required]),
    confirmPassword: new FormControl("", [Validators.required])
  })

  constructor(
    private utilsService : UtilsService,
    private _accountService: AccountService,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this.signUpForm.reset()
  }

  handleSubmit() {
    console.log("submit pressed")
    console.log(this.signUpForm)
    if (this.signUpForm.valid) {
      let data = this.signUpForm.value;
      
      if (data.password === data.confirmPassword) {
        // proceed to hit the API
        let stringifyedData = JSON.stringify(this.signUpForm.value);
        console.log("Data after validation", stringifyedData)
        let encodedData = this.utilsService.encode(stringifyedData)
        // let decodedData = this.utilsService.decode(data)
        console.log("Encoded Data", encodedData)
        // console.log("Decoded Data", decodedData)
        let payload = {
          encryptPayload: encodedData,
        }
        this._accountService.userMail = data.email;
        this._accountService.createAccount(payload).subscribe(
          (res: any) => {
            console.log("User Account creation response", res)
            if (res.data[0].isEmailExist) {
              this.showEmailExistErr = true;
              return;
            }
            this._accountService.user_ID = res.data[0].id;
            this._accountService.sendOTP(this._accountService.user_ID).subscribe(
              (res: any) => {
                console.log("res from Send OTP", res);
                this._router.navigate(['OTPverfiy']);
              }
            )
          }
        )
        // this.showOTPcomponent = true;
      } else {
        this.showPasswordErr = true;
        return;
      }
    } else {
      return;
    }
  }

  // handleOTPcancel(eve: boolean) {
  //   this.showPasswordErr = false;
  //   this.signUpForm.reset();
  //   this.showOTPcomponent = eve;
  // }

}
