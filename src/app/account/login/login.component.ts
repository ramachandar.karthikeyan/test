import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/account.service';
import { UtilsService } from 'src/app/utils.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide: boolean = true;
  showErr: boolean = false;
  mailNotVerified: boolean = false;
  Errmsg: string = "";
  loginForm: FormGroup = new FormGroup({
    email: new FormControl("", [Validators.required, Validators.email]),
    password: new FormControl("", [Validators.required])
  })

  constructor(
    private utilsService : UtilsService,
    private _accountService: AccountService,
    private _router : Router
  ) { }

  ngOnInit(): void {
    this.loginForm.reset()
  }
  

  handleSubmit() {
    console.log("submit pressed")
    console.log(this.loginForm)

    if (this.loginForm.valid) {
      let data = this.loginForm.value
      let stringifyedData = JSON.stringify(this.loginForm.value);
      let encodedData = this.utilsService.encode(stringifyedData)
      let payload = {
        encryptPayload: encodedData,
      }
      this._accountService.userMail = data.email;

      this._accountService.getUser(payload).subscribe(
        (res: any) => {
          console.log("User Login Response", res)
          if (res.data) {
            if (res.data.iscredentialsDidntMatch) {
              this.Errmsg = res.message; 
              this.showErr = true;
              return;
            }
            if (res.data.isnotVerified) {
              this.mailNotVerified = true;
              this._accountService.user_ID = res.data.id;
              return;
            }
            this._accountService.user_ID = res.data.id;
            this._accountService.userDetails = res.data;
            localStorage.setItem("token", res.data.token);
            this._router.navigate(["employee"])
          }else {
            this.Errmsg = res.message; 
            this.showErr = true;
          }
        }
      )
    } else {
      return;
    }
  }

  verfiyMail() {
    this._accountService.sendOTP(this._accountService.user_ID).subscribe(
      (res) => {
        this._router.navigate(['OTPverfiy'])
      }
    );
  }

}
