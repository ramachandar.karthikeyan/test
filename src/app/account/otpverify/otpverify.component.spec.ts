import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OTPverifyComponent } from './otpverify.component';

describe('OTPverifyComponent', () => {
  let component: OTPverifyComponent;
  let fixture: ComponentFixture<OTPverifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OTPverifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OTPverifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
