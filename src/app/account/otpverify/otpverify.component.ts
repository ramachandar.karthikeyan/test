import { Component, OnInit, ViewChildren, ElementRef, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/account.service';

@Component({
  selector: 'app-otpverify',
  templateUrl: './otpverify.component.html',
  styleUrls: ['./otpverify.component.scss']
})
export class OTPverifyComponent implements OnInit {
  formInput = ['input1', 'input2', 'input3', 'input4', 'input5', 'input6'];
  @ViewChildren('formRow') rows: any;
  // @Output() cancelEmitter: EventEmitter<boolean> = new EventEmitter();
  form: FormGroup;
  errMSg: string = "";
  notValid: boolean = false;
  constructor(
    public _accountService: AccountService,
    private router: Router
  ) {
    this.form = this.toFormGroup(this.formInput);
  }

  ngOnInit(): void {
  }

  toFormGroup(elements: any) {
    const group: any = {};
    elements.forEach((key: any) => {
      group[key] = new FormControl('', Validators.required);
    });
    return new FormGroup(group);
  }

  keyUpEvent(event: any, index: number) {
    let pos = index;
    if (event.keyCode === 8 && event.which === 8) {
     pos = index - 1 ;
    } else {
     pos = index + 1 ;
    }
    if (pos > -1 && pos < this.formInput.length ) {
     this.rows._results[pos].nativeElement.focus();
    }
  }

  cancel() {
    // this.cancelEmitter.emit(false)
    this.router.navigate(["sign-up"])
  }

  validate() {
    console.log("otp form", this.form)
    // this._accountService.verifyOTP()
    let otp = "";
    let valarr = Object.values(this.form.value);
    for (let val of valarr) {
      otp += val;
    }
    if (!this._accountService.user_ID) {
      this.router.navigate(['login']);
      return;
    }
    this._accountService.verifyOTP(this._accountService.user_ID, {otp: otp}).subscribe(
      (res: any) => {
        console.log("verify OTP RES", res);
        if (res.username && res.username === "OTP verification failed") {
          this.errMSg = "Verfication failed try resending code"
          this.notValid = true;
        }
        if (res.data && res.data.isOTPverified) {
          this.router.navigate(['/employee'])
        }
      },
      (err) => {
        console.log(err);
        this.errMSg = "Verfication failed try resending code"
        this.notValid = true;
      }
    );
  }

  resend() {
    this.notValid = false;
    this.form.reset();
    this._accountService.sendOTP(this._accountService.user_ID).subscribe(
      (res) => {

        
      }
    )
  }

}
